<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Jesal Chitala</title>

    <!-- Bootstrap -->
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="css/font-awesome.css" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick-theme.min.css" type="text/css">


    <!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <link rel="stylesheet" href="css/responsive.css" type="text/css">
    <link rel="stylesheet" href="css/animate.css" type="text/css">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        .header {
            position: relative;
            z-index: 9999;
        }

        .navbar-default .navbar-brand, .navbar-default .navbar-brand:hover, .navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > li > a ,.navbar-default .navbar-nav > .active > a:hover, .navbar-default .navbar-nav > .active > a:focus, .navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus{
            color: #fff;
        }

        /*
                .top-section {
                    background-color: #383339;
                    position: absolute;
                    top: 0;
                    padding: 150px 0px 30px;
                    width: 100%;
                }

                .top-section-content {
                    margin: 0px;
                }

                .footer {
                    position: fixed;
                    bottom: 0;
                    width: 100%;
                }

                .top-section-content h1 {
                    color: #fff;
                }
        */
    </style>
</head>
<body>
<!--- headr-top start --->
<div class="header-top work-header">
    <!--- headr start --->
    <div class="header">
        <nav class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="{{url('/')}}" class="navbar-brand">Jesal Chitala</a>
                </div>
                <!-- Collection of nav links and other content for toggling -->
                <div id="navbarCollapse" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="{{url('/')}}">About Me</a></li>
                        <li class="active"><a href="{{url('/work')}}">Work</a></li>
                        <li><a href="{{url('/process')}}">Process</a></li>
                        <li><a href="{{url('/contact')}}">Contact</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <!--- headr end --->
</div>
<!--- headr-top end --->

<div class="slider slider-for">

    <div class="top-section work crossbow">
        <div class="container">
            <div class="top-section-content work-content">
                <h1><strong>The Crossbow Shop</strong></h1>
                <h3>Product Based e-commerce website</h3>
                <p>Blockbox is a Saas (Software as Service) web application which allows people to collect information and store in a single place. A person can create a single documents, store images, links to websites, and collaborate with others.</p>
                <p><span><strong>YEAR:</strong> 2016</span> <span><strong>ROLE:</strong> UX/UI Design</span></p>
            </div>
        </div>
    </div>

    <div class="top-section work sbi">
        <div class="container">
            <div class="top-section-content work-content">
                <h1><strong>SBI Mutual Fund</strong></h1>
                <h3>Itenerary design for SBI MD's conforence</h3>
                <p>Agency brief was to design an iternary for State banks managing director's annual conforence happening in New Zealand. The State Bank of India is an Indian multinational, public sector bank.</p>
                <p><span><strong>Year:</strong> 2016</span> <span><strong>Year:</strong> Graphic Design</span></p>
            </div>
        </div>
    </div>

    <div class="top-section work auto">
        <div class="container">
            <div class="top-section-content work-content">
                <h1><strong>Auto Riskshwa App</strong></h1>
                <h3>Experiance Design For Auto-Transist</h3>
                <p>This thesis project began as a research study of the auto rickshaw transit issues faced by daily commuters in Mumbai City and investigated how design can improve the experienced auto rickshaw users in the city.</p>
                <p><span><strong>Year:</strong> 2017</span> <span><strong>Year:</strong> Thesis Project</span></p>
            </div>
        </div>
    </div>

    <div class="top-section work pizzagirl">
        <div class="container">
            <div class="top-section-content work-content">
                <h1><strong>PizzaGirls</strong></h1>
                <h3>Project based on Pizza delivery application</h3>
                <p>PizzaGirls is upcoming pizza dining brand with more than 10 franchises across Nova Scotia serving unique and delicious menu items such as gourment pizzas and pastas, juicy burgers and famous poutin.</p>
                <p><span><strong>Year:</strong> 2018</span> <span><strong>Year:</strong> Freelancer</span></p>
            </div>
        </div>
    </div>

    <div class="top-section work isle">
        <div class="container">
            <div class="top-section-content work-content">
                <h1><strong>ISLE</strong></h1>
                <h3>Environmental design project</h3>
                <p>The ISLE Project proposes the design and implementation of a temporary architectural installation that performs as an instrument to enhance our perception of the coastal landscape. It is sited in the unique archipelago of the 100 wild islands on the estaern shore of Nova Scotia.</p>
                <p><span><strong>Year:</strong> 2017</span> <span><strong>Year:</strong> Communication Design</span></p>
            </div>
        </div>
    </div>

    <div class="top-section work propisify">
        <div class="container">
            <div class="top-section-content work-content">
                <h1><strong>Proposify</strong></h1>
                <h3>Project based on feature development</h3>
                <p>Proposify revolunizies the praposal process. Product Mission is to make proposal process simple for small to large scale business.</p>
                <p><span><strong>Year:</strong> 2016</span> <span><strong>Year:</strong> Graphic Design</span></p>
            </div>
        </div>
    </div>

    <div class="top-section work forindia">
        <div class="container">
            <div class="top-section-content work-content">
                <h1><strong>FORINDIA</strong></h1>
                <h3>Visual Identity & Branding Project</h3>
                <p>Creating and designing an identity for compnay governed by Govt. of india dealing ? with all agricultural farming activities. This Project was created under mentorship of Prof. Mahendra Patel at Symbiosis Institue od Design.</p>
                <p><span><strong>Year:</strong> 2014</span> <span><strong>Year:</strong> Visual Design & Branding</span></p>
            </div>
        </div>
    </div>

</div>

<div class="slider slider-nav">
    <div>

        <div class="work-img-boxs">
            <img src="http://placehold.it/1920x1000" class="img-responsive">
        </div>

        <div class="container default-padding work-wrapper">
            <div class="work-img-box">
                <img src="http://placehold.it/1500x2400" class="img-responsive">
            </div>

            <div class="work-img-box">
                <img src="http://placehold.it/1500x1800" class="img-responsive">
            </div>

            <div class="work-img-box">
                <img src="http://placehold.it/1500x1600" class="img-responsive">
            </div>

            <div class="work-img-box">
                <img src="http://placehold.it/1500x1000" class="img-responsive">
            </div>
        </div>
    </div>

    <div>

        <div class="work-img-boxs">
            <img src="http://placehold.it/1920x1000" class="img-responsive">
        </div>

        <div class="container default-padding work-wrapper">

            <div class="adj-with">
                <h3>This project was assigned to me by my creative director while I worked at Rediffusion Y&R in Mumbai city. I specially designed information communication & graphic for SBI as it was my agenices biggest client. The bank has certain manadates over its branding & communication styles. This brochure was to be handed over to the Managing directors of the bank. Goal was to provided them detailed information regarding their conference agenda in NZ. </h3>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1000" class="img-responsive">
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1000" class="img-responsive">
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1000" class="img-responsive">
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1000" class="img-responsive">
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1000" class="img-responsive">
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1000" class="img-responsive">
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1000" class="img-responsive">
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1000" class="img-responsive">
                </div>
            </div>
        </div>
    </div>

    <div>
        <div class="container default-padding work-wrapper">

            <div class="adj-with">


                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1800" class="img-responsive">
                </div>

                <h3>Since the development of auto-rickshaws in India in the late 1950s, these vehicles have become an indispensable part of urban mobility for millions of people. Auto rickshaws play a vital and vigorous role in India's urban transport systems. They also represent a very improvisational and increasingly inefficient sector — and they are getting lost in the changing dynamics of urban mobility in India.</h3>

                <h3>Today, with increasing urban populations, there is growth in demand for urban transport, growth in private motorization and a decline in public transport share. How do auto rickshaws fit in and have a role that is efficient — for both the operators and their passengers? And how can this mode of paratransit contribute to urban transport sustainability — through both increasing efficiency and safety for everyone on the roads?</h3>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1000" class="img-responsive">
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1000x500" class="img-responsive">
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1000x700" class="img-responsive">
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1000x700" class="img-responsive">
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1000x700" class="img-responsive">
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1000x700" class="img-responsive">
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1000x700" class="img-responsive">
                </div>
            </div>
        </div>
    </div>

    <div>
        <div class="container default-padding work-wrapper">

            <div class="adj-with">

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1000" class="img-responsive">
                </div>

                <h3>The client wanted to setup the pizza business online as it gets much easier for customers to give pizza orders online and recieve delivery at doorsteps. My goal was to design a pizza delivery portal which would help users to order & pay for food online. Also the users should be able to customize their orders. As a freelancer I provided the client with a digital roadmap and also helped them to manage this project. I assembled a team of photographer, engineer & content designer to execute the project.</h3>

                <h1>The Landing Page</h1>

                <div class="work-img-box">
                    <img src="http://placehold.it/1200x2200" class="img-responsive">
                </div>

                <h1>Menu Page</h1>

                <div class="work-img-box">
                    <img src="http://placehold.it/1200x3000" class="img-responsive">
                </div>

                <h1>Item Description & Customization Options</h1>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1000" class="img-responsive">
                </div>

                <h1>Creating Your Own Pizza Process</h1>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1000" class="img-responsive">
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1400" class="img-responsive">
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1200x2200" class="img-responsive">
                </div>

                <h1>Account Setting & Payment Page</h1>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1400" class="img-responsive">
                </div>

                <h1>Ordering Page</h1>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1000" class="img-responsive">
                </div>
            </div>
        </div>
    </div>

    <div>
        <div class="container default-padding work-wrapper">
            <div class="adj-with">

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1400" class="img-responsive">
                </div>

                <div class="rows">
                    <h1>Project Brief</h1>

                    <div class="col-md-6">
                        <h3>This Project was taken up during my freelab course at Dalhousie architecture school. The ISLE Project proposes the design & implementation of a temporary architectural installation that performs as an instrument to enhance our perception of the coastal landscape. It is sited in the unique archipelago of the 100 Wild Islands on the eastern shore of Nova Scotia.</h3>
                    </div>

                    <div class="col-md-6">
                        <h3>Our team explored how mechanics, repetition, materials and position can be employed to interpret and express physical qualities of a site. Research into the geomorphic history, hydrology, ecology and climate patterns of the 100 Wild Islands archipelago, as well as an immersive pre-construction camping trip informed our understanding of the context and inspire the work.</h3>
                    </div>
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x500" class="img-responsive">
                </div>
            </div>
        </div>

        <div class="work-img-box">
            <img src="http://placehold.it/1920x1200" class="img-responsive">
        </div>

        <div class="container work-wrapper">
            <div class="adj-with">
                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1600" class="img-responsive">
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1600" class="img-responsive">
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1600" class="img-responsive">
                </div>
            </div>
        </div>

        <div class="work-img-boxs">
            <img src="http://placehold.it/1920x400" class="img-responsive">
        </div>
        <div class="work-img-boxs">
            <img src="http://placehold.it/1920x400" class="img-responsive">
        </div>
        <div class="work-img-box">
            <img src="http://placehold.it/1920x400" class="img-responsive">
        </div>

        <div class="container work-wrapper">
            <div class="adj-with">
                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1700" class="img-responsive">
                </div>
            </div>
        </div>

        <div class="work-img-boxs">
            <img src="http://placehold.it/1920x1600" class="img-responsive">
        </div>
        <div class="work-img-boxs">
            <img src="http://placehold.it/1920x1600" class="img-responsive">
        </div>
    </div>

    <div>
        <div class="container default-padding work-wrapper">

            <div class="adj-with">
                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1000" class="img-responsive">
                </div>

                <h3>Proposify is a product to create & design winning contract deals for small scale to large scale businesses. Some of their core features are they have beautiful templates for various kinds of businesses. They also make the entire signing process easier with some team management functions. During my work term with Proposify I worked on various projects based on new feature development for the application, some of these can be experienced by signing up for the application. Below are some of the UX practices & solutions I achieved.</h3>

                <div class="rows">
                    <h1>Project Brief</h1>

                    <div class="col-md-7 pd-left">
                        <h1 class="bigheadng">Developing a new feature for proposal audit trails </h1>

                        <div class="para-box">
                            <p class="para">Each signature on a contract is imposed and affixed to the document. When you request a signature, Proposify affixes an audit trail page to the document itself. The audit trail contains a globally unique identifier, or GUID, that can be used to look up a record in our database that shows who signed a document and when. These records include a hash of the PDF document which we can compare to the hash of a questionable PDF document to determine whether or not it has been modified or tampered with. Our statement of legality can be admisible in court and provides a detailed audit log to our users.</p>

                            <p class="para">The non-editable audit trail ensures that every action on your documents is thoroughly tracked and time-stamped, to provide defensible proof of access, review, and signature.</p>
                        </div>

                        <div class="box-next">
                            <h3>Information Architecture</h3>

                            <p>These diagrams depict how audit trail feature is adapted to the existing functionality of the application. I attained these solutions by understanding & analysising the existing structure and signing process in the app. In the following models below I revised the flow by creating different scenarios and usage of audit trail function.</p>
                        </div>
                    </div>

                    <div class="col-md-5">
                        <div class="work-img-box">
                            <img src="http://placehold.it/500x1000" class="img-responsive">
                        </div>
                    </div>
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1600" class="img-responsive">
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1600" class="img-responsive">
                </div>

                <h3>Integreting th IA into Wireframes</h3>

                <p>The wireframes below present the the structure of the audit trail feature and how it will adapt into the existing interface of the software. These wireframes also help various teams to understand the process & functionality of the feature. </p>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1600" class="img-responsive">
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1600" class="img-responsive">
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1200" class="img-responsive">
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1200" class="img-responsive">
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x1200" class="img-responsive">
                </div>
            </div>
        </div>
    </div>

    <div>
        <div class="container work-wrapper">
            <div class="adj-with">
                <div class="work-img-box">
                    <img src="http://placehold.it/1500x800" class="img-responsive">
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x800" class="img-responsive">
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x800" class="img-responsive">
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x800" class="img-responsive">
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x800" class="img-responsive">
                </div>

                <div class="work-img-box">
                    <img src="http://placehold.it/1500x800" class="img-responsive">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<!--- Footer start --->
<footer class="footer footer-padding">
    <div class="container">
        <div class="row">
            <div class="footer-content">
                <div class="col-md-6">
                    <h1>Jesal Chitala</h1>
                </div>

                <div class="col-md-6">
                    <ul class="links pull-right">
                        <li><a href="{{url('/work')}}">Work</a></li>
                        <li><a href="{{url('/')}}">About</a></li>
                        <li><a href="{{url('/contact')}}">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="footer-content bottom-footer">
                <div class="col-md-6">
                    <p>Design by @Jesal Chitalia. All Rights Reserved.</p>
                </div>

                <div class="col-md-6">
                    <ul class="social pull-right">
                        <li><a href=""><i class="fa fa-facebook"></i></a></li>
                        <li><a href=""><i class="fa fa-pinterest"></i></a></li>
                        <li><a href=""><i class="fa fa-twitter"></i></a></li>
                        <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--- Footer end --->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<scrip src="js/jquery.1.11.1.js"></scrip>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/main.js"></script>
</body>
</html>