// Hello.
//
// This is The Scripts used for ___________ Theme
//
//

$(document).ready(function() {
  $('.collapse.in').prev('.panel-heading').addClass('active');
  $('#accordion, #bs-collapse')
    .on('show.bs.collapse', function(a) {
      $(a.target).prev('.panel-heading').addClass('active');
    })
    .on('hide.bs.collapse', function(a) {
      $(a.target).prev('.panel-heading').removeClass('active');
    });
});


/*** work slider ***/

$(document).ready(function(){
    $('.slider-for').slick({
       slidesToShow: 1,
       slidesToScroll: 1,
       arrows: true,
       fade: true,
       dots: false,
        asNavFor: '.slider-nav',
//        prevArrow: '<button class="slide-arrow slide-prev"></button>',
//        nextArrow: '<button class="slide-arrow slide-prev"></button>'
     });
    
    $('.slider-nav').slick({
       slidesToShow: 1,
       slidesToScroll: 1,
      arrows: false,
       asNavFor: '.slider-for',
       dots: false,
        fade: true,
       autoplay: false,
       focusOnSelect: true,
         adaptiveHeight: true
     });

     $('a[data-slide]').click(function(e) {
       e.preventDefault();
       var slideno = $(this).data('slide');
       $('.slider-nav').slick('slickGoTo', slideno - 1);
     });
});